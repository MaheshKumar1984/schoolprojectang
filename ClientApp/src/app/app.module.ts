import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { MastersComponent } from './masters/masters.component';
import { StudentComponent } from './student/student.component';
import { ExaminationComponent } from './examination/examination.component';
import { FeesComponent } from './fees/fees.component';
import { EmployeeComponent } from './employee/employee.component';
import { SubjectassigmentComponent } from './subjectassigment/subjectassigment.component';
import { ReportsComponent } from './reports/reports.component';
import { LoginComponent } from './login/login.component';
import { SubjectComponent } from './masters/subject/subject.component';
import { ExamtypeComponent } from './masters/examtype/examtype.component';
import { CityComponent } from './masters/city/city.component';
import { DesignationComponent } from './masters/designation/designation.component';
import { LanguageComponent } from './masters/language/language.component';
import { ProfessionComponent } from './masters/profession/profession.component';
import { QualificationComponent } from './masters/qualification/qualification.component';
import { RelationshipComponent } from './masters/relationship/relationship.component';
import { ReligionComponent } from './masters/religion/religion.component';
import { StateComponent } from './masters/state/state.component';
import { HttpClientModule } from '@angular/common/http';
import { MenubarComponent } from './menubar/menubar.component';
import { PageheaderComponent } from './pageheader/pageheader.component';
import { HomeComponent } from './home/home.component';
import { ClassComponent } from './masters/class/class.component';
import { SchoolComponent } from './masters/school/school.component';
import { SessionComponent } from './masters/session/session.component';

import { AccordionModule } from 'primeng/accordion';     //accordion and accordion tab
import { MenuItem } from 'primeng/api';                  //api
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';                  //api
import { PaginatorModule } from 'primeng/paginator';
import {MenubarModule} from 'primeng/menubar';

import {InputTextModule} from 'primeng/inputtext';
import {ToolbarModule} from 'primeng/toolbar';
import {ToastModule} from 'primeng/toast';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {ContextMenuModule} from 'primeng/contextmenu';
import { LazyLoadEvent } from 'primeng/api';

@NgModule({
  declarations: [
    AppComponent,
    MastersComponent,
    StudentComponent,
    ExaminationComponent,
    FeesComponent,
    EmployeeComponent,
    SubjectassigmentComponent,
    ReportsComponent,
    LoginComponent,
    SubjectComponent,
    ExamtypeComponent,
    CityComponent,
    DesignationComponent,
    LanguageComponent,
    ProfessionComponent,
    QualificationComponent,
    RelationshipComponent,
    ReligionComponent,
    StateComponent,
    MenubarComponent,
    PageheaderComponent,
    HomeComponent,
    ClassComponent,
    SchoolComponent,
    SessionComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,    
    BrowserAnimationsModule,
    TableModule,
    HttpClientModule,
    ButtonModule,
    PaginatorModule,
    MenubarModule,
    InputTextModule,
    ToolbarModule,
    ToastModule,
    DialogModule,
    DropdownModule,
    ContextMenuModule
    
   


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
