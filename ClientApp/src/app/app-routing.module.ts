import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
//import { EmployeeComponent } from './employee/employee.component';
//import { ExaminationComponent } from './examination/examination.component';
//import { FeesComponent } from './fees/fees.component';
//import { LoginComponent } from './login/login.component';
import { MastersComponent } from './masters/masters.component';
import { ExaminationComponent } from './examination/examination.component';
import { FeesComponent } from './fees/fees.component';
import { EmployeeComponent } from './employee/employee.component';
import { SubjectassigmentComponent } from './subjectassigment/subjectassigment.component';
import { ReportsComponent } from './reports/reports.component';
import { LoginComponent } from './login/login.component';
import { SubjectComponent } from './masters/subject/subject.component';
import { ExamtypeComponent } from './masters/examtype/examtype.component';
import { CityComponent } from './masters/city/city.component';
import { DesignationComponent } from './masters/designation/designation.component';
import { LanguageComponent } from './masters/language/language.component';
import { ProfessionComponent } from './masters/profession/profession.component';
import { QualificationComponent } from './masters/qualification/qualification.component';
import { RelationshipComponent } from './masters/relationship/relationship.component';
import { ReligionComponent } from './masters/religion/religion.component';
import { StateComponent } from './masters/state/state.component';
import { HomeComponent } from './home/home.component';
import { ClassComponent } from './masters/class/class.component';
import { SchoolComponent } from './masters/school/school.component';
import { SessionComponent } from './masters/session/session.component';

const routes: Routes = [
  { path: 'masters', component: MastersComponent },
  { path: 'subject', component: SubjectComponent },
  { path: 'examtype', component: ExamtypeComponent },
  { path: 'city', component: CityComponent },
  { path: 'designation', component: DesignationComponent },
  { path: 'language', component: LanguageComponent },
  { path: 'profession', component: ProfessionComponent },
  { path: 'qualification', component: QualificationComponent },
  { path: 'relationship', component: RelationshipComponent },
  { path: 'examination', component: ExaminationComponent },
  { path: 'fees', component: FeesComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: '*', component: LoginComponent },
  {path:'home',component:HomeComponent},
  { path: 'class', component: ClassComponent },
  { path: 'school', component: SchoolComponent },
  { path: 'session', component: SessionComponent },
  { path: 'employee', component: EmployeeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
