import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectassigmentComponent } from './subjectassigment.component';

describe('SubjectassigmentComponent', () => {
  let component: SubjectassigmentComponent;
  let fixture: ComponentFixture<SubjectassigmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubjectassigmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectassigmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
