import { Component, OnInit } from '@angular/core';
import { CommonlistService } from '../../SERVICES/MASTERS/commonlist/commonlist.service'
import * as XLSX from 'xlsx'
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css'],
  providers: [CommonlistService]
})
export class SubjectComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    body: new FormControl('', Validators.required)
  });
  
  get f(){
    return this.form.controls;
  }
  
  submit(){
    this.DBMessage="Record already exist.";
    console.log(this.form.value);
  }
  subjectlist: any = [];
  first = 0;
  rows = 10;
  cols: any[]=[];
  exportColumns: any[]=[];
  filename:string="SubjectList.xlsx"
  dialogdisplay:boolean= false;
  submitted:boolean=false;

  DBMessage:string="";

  constructor(private http: CommonlistService) { }

  ngOnInit(): void {
    debugger;
    this.getSubjectlist();     
  }

  getSubjectlist() {
   
    this.http.getSubjectList().then(result => {   
      console.log(result);
      this.subjectlist = result;
    });
    //this.subjectlist=[{subjecT_ID:7,subjecT_NAME:'ENGLISH'},{subjecT_ID:8,subjecT_NAME:'Hindi'},{subjecT_ID:9,subjecT_NAME:'Maths'}];
  }

  exportExcel():void {

    let elemnet =document.getElementById('table-excel');
    const ws:XLSX.WorkSheet=XLSX.utils.table_to_sheet(elemnet);
    const wb:XLSX.WorkBook=XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb,ws,'Sheet1');
    XLSX.writeFile(wb,this.filename);
  }

  AddNew() {
    this.dialogdisplay=true;  
  }
  cancle() {
    this.dialogdisplay=false;
    
  }


  editSubject(subjects:any) {
    console.log(subjects);
    this.dialogdisplay=true;  
  }

  deleteSubject(subjects:any) {
  
  }



  // get f() { return this.subjectForm.controls; }

  // onSubmit() {
  //   debugger;
  //     this.submitted = true;   
  //     if (this.subjectForm.invalid) {
  //         return;
  //     }
  //     alert('SUCCESS!! :-)')
  // }
}
