import { Component, OnInit } from '@angular/core';
import {CommonlistService } from '../SERVICES/MASTERS/commonlist/commonlist.service'

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.css'],
  providers: [CommonlistService]

})
export class MastersComponent implements OnInit {

  constructor(private http: CommonlistService) { }

  ngOnInit(): void {
    this.http.getSubjectList();
  }

}
