import { TestBed } from '@angular/core/testing';

import { CommonlistService } from './commonlist.service';

describe('CommonlistService', () => {
  let service: CommonlistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonlistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
