import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CommonlistService {
  private headers: HttpHeaders;
  constructor(private http:HttpClient) {
    this.headers = new HttpHeaders();
  }

  getSubjectList() {
    debugger;
    let url = 'https://localhost:44318/api/masters';
    //url = url.replace(/([^:])(\/\/+)/g, '$1/');
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(url).toPromise()
      .then(res=>res)
      .then(data => {
        console.log(data);
        return data;
      });
  }
}
