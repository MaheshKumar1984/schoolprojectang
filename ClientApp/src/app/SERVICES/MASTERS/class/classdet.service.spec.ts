import { TestBed } from '@angular/core/testing';

import { ClassdetService } from './classdet.service';

describe('ClassdetService', () => {
  let service: ClassdetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClassdetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
